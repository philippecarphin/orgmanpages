#!/bin/bash
set -eEuo pipefail
usage(){
    cat <<- EOF
		USAGE: $0 SRC DST
	EOF
    if [[ "${1-}" != full ]] ; then
        cat <<- EOF
			ARGUMENTS:
			    SRC: directory containing a directory 'share' containing a manpage tree of org or markdown files
			    DST: a target directory into which a directory 'share' will be reproduced

			echo "run '$0 --help' for more details"
		EOF
    else
        cat <<- EOF

			First argument SRC must contain a tree of the form

			    SRC/
			    └── share
			        └── man
			            ├── man1
			            │   ├── git-ignore.org
			            │   └── git-yaml.org
			            ├── man3
			            │   ├── librmn.org
			            │   ├── manpage-template.org
			            │   └── my_c_library_function.org
			            ├── man4
			            │   └── silent-thunder.org
			            └── man5
			                └── fst.org

			This structure is imposed due to the fact that files must be organized
			this way for the 'man' program to find them.  Notice how the extensions
			match the section numbers in the output tree, this is also necessary.

			    DST/
			    └── share
			        └── man
			            ├── man1
			            │   ├── git-ignore.1
			            │   └── git-yaml.1
			            ├── man3
			            │   ├── librmn.3
			            │   ├── manpage-template.3
			            │   └── my_c_library_function.3
			            ├── man4
			            │   └── silent-thunder.4
			            └── man5
			                └── fst.5

			For the manpages in DST to be findable, either

			- DST/bin is in \$PATH and either
			  - \$MANPATH is not defined
			  - \$MANPATH is defined and contains a leading colon, trailing colon, or
			    double colon (':P1:P2:P3', 'P1:P2:P3:', or 'P1::P2:P3')
			- DST/share/man is in \$MANPATH
		EOF
    fi
}

# extension=md
# pandoc_input_format=markdown

extension=org
pandoc_input_format=org

# Assumes ${source_dir} contains /share/man/man<n> directories each containing
# orgmode files
main()
{
    for a in "$@" ; do case $a in
        -h) usage; exit 0 ;;
        --help) usage full ; exit 0 ;;
    esac ; done
    error_checks "$@"
    manpagerize "$@"
}

manpagerize(){
    set -x
    shopt -s nullglob
    source_dir=${1}
    install_dir=${2}
    sections=($(printf "${source_dir}/share/man/man%d\n" {1..8}))
    for section in "${sections[@]}" ; do
        section_number=${section##*man/man}
        echo "section_number=$section_number"
        mkdir -p ${install_dir}/share/man/man${section_number}
        for f in ${section}/*.org ; do
            base_with_ext=${f##${section}/}
            name=${base_with_ext%%.org}
            dst=${install_dir}/share/man/man${section_number}/${name}.${section_number}
            if ! pandoc -s -f org -t man ${f} -o ${dst} ; then
                echo "Error converting input file '${f}' from '${pandoc_input_format}' to manpage"
                return 1
            fi
        done
    done
}

error_checks(){
    if [[ $# != 2 ]] ; then
        echo "ERROR: Two arguments are required : SRC DST"
        usage
        exit 1
    fi
    if ! [[ -d ${1}/share ]] ; then
        echo "ERROR: SRC is expected to contain a directory named 'share'"
        usage
        exit 1
    fi

    if ! [[ -d ${1}/share/man ]] ; then
        echo "SRC is expected to contain a directory named 'share/man'"
        usage
        exit 1
    fi

    if ! find ${1}/share/man -name 'man*' -type d | grep "${1}/share/man/man[1-7]$" --silent ; then
        echo "SRC is expected to contain at least one directory of the form 'share/man/man[1-7]'"
        usage full
        exit 1
    fi

    local err
    if ! err=$(mkdir -p ${2} 2>&1); then
        echo "DST '${2}' cannot be created: $err"
        exit 1
    fi
}

main "$@"
