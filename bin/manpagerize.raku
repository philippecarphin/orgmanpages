#!/usr/bin/env raku

# share
# └── man
#     ├── man1
#     │   ├── git-ignore.org
#     │   ├── git-yaml.org
#     │   └── manpagerize.org
#     ├── man3
#     │   ├── librmn.org
#     │   ├── manpage-template.org
#     │   └── my_c_library_function.org
#     ├── man4
#     │   └── silent-thunder.org
#     └── man5
#         └── fst.org

sub MAIN( Str :$src #= A directory containing a 'share' tree with manpage sources
        , Str :$dst #= A directory where the tree structure of 'share' from SRC will be reproduced with manpages generated from the sources in the original tree
        , Str :$format   #= Format [org, markdown]
        , Bool :$dry-run = False #= Only print commands
        )
{
    if not $src.IO.d {
        say "ERROR: Expecting PWD to contain a directory named 'share'";
        exit 1;
    }

    my %extension_map := {
        'markdown' => 'md',
        'org' => 'org',
    };

    my %extension_reverse_map := {
        '.md' => 'markdown',
        '.org' => 'org',
    }

    if not %extension_map{$format}:exists {
        say "ERROR: --format must be either 'org' or 'markdown'";
    }

    my $extension = ".{%extension_map{$format}}";

    for dir("$src/share/man") -> $section {
        my $section-number = $section.substr(*-1);
        my $rel-section = $section.substr($src.chars);
        if $dry-run {
            say "mkdir $dst/$rel-section"
        } else {
            mkdir "$dst/$rel-section";
        }
        for dir($section) -> $source-file {
            my $rel-file = $source-file.substr($src.chars);
            my $rel-name = $rel-file.substr(0,*-$extension.chars);
            # if endswith 
            my $cmd = "pandoc -s -f $format -t man $source-file -o $dst/$rel-name.$section-number";
            if $dry-run {
                say $cmd;
            } else {
                shell $cmd;
            }
        }
    }

}
