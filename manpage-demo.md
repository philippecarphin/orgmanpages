% manpage-demo

# NAME

manpage-demo - This is a demo of manpages

# DESCRIPTION

This is a markdown file that will be converted to markdown

# COMMANDS

## CONVERTING TO MAN FORMAT

Converting to man format is best done with pandoc (`brew install pandoc`):
```
pandoc -s -f markdown -t man manpage-demo.md -o manpage-demo.man
```

## READING MANPAGES

Man uses the environment variables `PATH` and `MANPATH` to find manpages, more
on that later.  But for this demo, we just want to open our converted file, all
we have to do is provide a path containing a `/` and `man` will bypass its search
and just open the specified file:
```
man ./manpage-demo.man
```

# TITLE

The `% TITLE` thing at the top is a thing to tell `pandoc` what to put as the
title since markdown doesn't have a title parameter.

The other way to do it is to increase all your heading levels by 1 and tell pandoc
to decrease them all by 1.  And with that, the level 1 heading becomes a level 0
heading which gets used as the title

```
# manpage-demo

## NAME
## DESCRIPTION
## COMMANDS
### CONVERTING TO MAN FORMAT
...
```
and the pandoc command would be
```
pandoc -s -f markdown -t man --shift-heading-level-by=-1 manpage-demo.md manpage-demo.man
```

