
PREFIX ?= $(PWD)/localinstall

ifeq ($(shell uname),Darwin)
	# If you are on a mac and don't have ginstall, run 'brew install coreutils'
	INSTALL=ginstall
else
	INSTALL=install
endif

.PHONY: man
# Takes ${arg1}/share/man/man*/*.org to ${arg2}/shre/man/man*/*.[1-7]
man:
	./manpagerize.sh . .

install:
	./manpagerize.sh . $(DESTDIR)$(PREFIX)


clean:
	rm -f *.ssm man/man1/*.man
	rm -rf localinstall
