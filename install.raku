#!/usr/bin/env raku

sub MAIN(
    Str :$dst #= Install destination of this repo
)
{
    mkdir "$dst/bin";
    copy "bin/manpagerize.raku", "$dst/bin/manpagerize";
    shell "bin/manpagerize.raku --src $*CWD --dst $dst --format org"
}
